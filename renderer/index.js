const { ipcRenderer } = require('electron');
const { $, convertDuration } = require('./help');
let musicAudio = new Audio();
let allTracks;
let currentTrack;

$('add-music-button').addEventListener('click', () => {
    ipcRenderer.send('add-music-window');
});

renderListHTML = (tracks) => {
    const tracksList = $('tracksList');
    const tracksItemHTML = tracks.reduce((html, track) => {
        html += `<li class="list-group-item row music-track d-flex align-items-center">
            <div class="col-10">
                <i class="fas fa-music mr-2 text-secondary"></i>
                <b>${track.fileName}</b>
            </div>
            <div class="col-2">
                <i class="fas fa-play mr-3" data-id="${track.id}"></i>
                <i class="fas fa-trash-alt" data-id="${track.id}"></i>
            </div>
        </li>`;
        return html;
    }, '');
    const emptyTrackHTML = `<div class="alert alert-primary">还没有添加任何音乐</div>`;
    tracksList.innerHTML = tracks.length ? `<ul class="list-group">${tracksItemHTML}</ul>` : emptyTrackHTML;
}

ipcRenderer.on('getTracks', (event, tracks) => {
    renderListHTML(tracks);
    allTracks = tracks;
})

$('tracksList').addEventListener('click', (event) => {
    event.preventDefault();
    const { dataset, classList } = event.target;
    const id = dataset && dataset.id;
    // 播放音乐
    if (id && classList.contains('fa-play')) {
        if (currentTrack && currentTrack.id === id) {
            // 继续播放
            musicAudio.play();
        } else {
            // 重新播放
            currentTrack = allTracks.find(track => track.id === id);
            musicAudio.src = currentTrack.path;
            musicAudio.play();
            const resetIconEle = document.querySelector('.fa-pause');
            if (resetIconEle) {
                resetIconEle.classList.replace('fa-pause', 'fa-play');
            }
        }
        classList.replace('fa-play', 'fa-pause');
        return;
    }
    // 暂停音乐
    if (id && classList.contains('fa-pause')) {
        musicAudio.pause();
        classList.replace('fa-pause', 'fa-play');
        return;
    }
    // 删除音乐
    if (id && classList.contains('fa-trash-alt')) {
        ipcRenderer.send('delete-track', id);
        return;
    }
})

renderPlayerHTML = (name, duration) => {
    const player = $('player-status');
    const PlayerHTML = `<div class="col font-weight-bold">
                            正在播放：${name}
                        </div>
                        <div class="col">
                            <span id="current-seeker">00:00</span> / ${convertDuration(duration)}
                        </div>`;
    player.innerHTML = PlayerHTML;
}

updateProgressHTML = (currentTime, duration) => {
    const seeker = $('current-seeker');
    seeker.innerHTML = convertDuration(currentTime);
    // 计算progress
    const progress = Math.floor(currentTime / duration * 100);
    const bar = $('player-progress');
    bar.innerHTML = progress + '%';
    bar.style.width = progress + '%';
}

musicAudio.addEventListener('loadedmetadata', () => {
    // 渲染播放器状态
    renderPlayerHTML(currentTrack.fileName, musicAudio.duration);
})

musicAudio.addEventListener('timeupdate', () => {
    // 更新播放器状态
    updateProgressHTML(musicAudio.currentTime, musicAudio.duration);
})